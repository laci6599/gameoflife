package gameoflife.lifereader;

import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public final class LifeReader {
    
    private LifeReader(){}
    
    public final static Pair<String,Point[]> readLif(File file) throws NotSupportedLifeVersionException, FileNotFoundException{   
    
        String rule = "23/3";
        ArrayList<Point> livingCells = new ArrayList<>();
        Point startPoint = null;
        Integer x = null;
        Integer y = null;
        
        try(Scanner sc = new Scanner(file)){
            
            String fn = file.getName();
            if(!fn.substring(fn.lastIndexOf(".") + 1, fn.length()).equals("LIF")) {
                throw new NotSupportedLifeVersionException("Only .LIF files are supported.");
            }
           
            while(sc.hasNext()){
                String line = sc.nextLine();
                if(line.matches("#D.*")){ continue;}  
                if(line.matches("#N.*")){ continue;} 
                if(line.matches("#R.*")){ rule = line.replaceAll("\\s+", "").substring(2); continue; } 
                if(line.matches("#P.*")){                                                              
                
                    String[] temp = line.split(" ");
                    x = Integer.valueOf(temp[1]);
                    y = Integer.valueOf(temp[2]);
                    startPoint = new Point(x, y);
                    continue;
                }
                if(line.matches("[\\*\\.]+")){
                    
                    for(char c:line.toCharArray()){
                        if(c == '.')     { x++; } 
                        else if(c == '*'){ livingCells.add(new Point(x++, y)); } 
                    }
                    
                    x = startPoint.x;
                    y++;            
                }
            }
        } 
            
        return (new Pair<>(rule, livingCells.toArray(new Point[livingCells.size()])) );
    }
}
